---
layout: markdown_page
title: "SOX PMO"
---

## On this page
{:.no_toc}

- TOC
{:toc}


## Attributes

| Property | Value |
|----------|-------|
| Date Created | March 11, 2019 |
| Date Ended   | TBD |
| Slack        | [#wg_SOX- PMO](https://gitlab.slack.com/messages/CGV47EU85) (only accessible from within the company) |
| Google Doc   | [SOX Working Group Agenda](https://docs.google.com/presentation/d/1po7k74p5cYThNlo4H6q_dgmBIMzezXW7tKNRC2BZQJs/edit?usp=sharing) (only accessible from within the company) 




## Roles and Responsibilities

| Role                  | Responsibility                                                          |
|-----------------------|-------------------------------------------------------------------------|
| Facilitator           | Sheetal Jain |
| Functional Lead       | Cindy Nunez, Melody Gallagher, Melissa Farber                                   |
| Member                | Ramakrishnan Hariharan, Jamie Hurewitz, Kathy Wang                                              |
| Executive Stakeholder | Paul Machle |


