--- 
layout: markdown_page
title: "All Remote"
---

## On this page
{:.no_toc}

- TOC
{:toc}

GitLab is an all-remote company. Just like you can control your television with a
tv remote with some level of location independence, you can work at GitLab from 
[almost anywhere in the world](/jobs/faq/#country-hiring-guidelines). 

## The Remote Manifesto

All-remote work promotes:

- Hiring and working from all over the world *instead of* from a central location. 
- Flexible working hours *over* set working hours.
- Writing down and recording knowledge *over* verbal explanations.
- Written down processes *over* on-the-job training. 
- Public sharing of information *over* need-to-know access.
- Opening up every document for editing by anyone *over* top-down control of documents. 
- Asynchronous communication *over* synchronous communication.
- The results of work *over* the hours put in. 
- Formal communication channels *over* informal communication channels. 

## Why remote?

>  **"Remote is not a challenge to overcome. It's a clear business advantage." -Victor Wu, Product Manager, GitLab**

From the cost savings on office space to more flexibility in employees' daily lives, all-remote work offers a number of advantages to organizations and their people. But we also recognize that being part of an all-remote company isn't for everyone. Here's a look at some of the advantages and disadvantages.  

### Advantages

#### *For employees*
- More flexibility in your daily life (for kids, parents, friends, groceries, sports, deliveries)
- No more time, stress, or money wasted on a commute (subway and bus fees, gas, car maintenance, tolls, etc.)
- Less exposure to germs from sick coworkers
- Reduced interruption stress and increased [productivity](https://www.inc.com/brian-de-haaff/3-ways-remote-workers-outperform-office-workers.html)
- Ability to travel to other places without taking vacation (family, fun, etc.)
- Freedom to relocate
- Some find it easier to communicate with difficult colleagues remotely
- Onboarding may be less stressful socially
- Eating at home is better (sometimes) and cheaper
- Taxes can be cheaper in some countries 
- Pants not required

From family time to travel plans, there are [many examples and stories](https://about.gitlab.com/company/culture/all-remote/stories/) of how remote work has impacted the lives of GitLab team members around the world.

#### *For your organization*
- Hire great people no matter where they live
- More productive employees with fewer distractions
- Increased savings on office costs, compensation (due to hiring in lower-cost regions), and taxes in some countries
- Naturally attracts self-starters
- Easier to quickly grow your company
- Fewer meetings and more focus on results and output of great work
- Business continuity in the case of local disturbances or natural disasters (e.g. political or weather-related events)

#### *For the world*

All-remote work has advantages beyond just one organization and its people. With no commuting employees and no office buildings or campuses, all-remote companies have a signifiantly smaller environmental footprint. For global companies, bringing better-paying jobs to low-cost regions also has positive economic impact. 

### Disadvantages

Despite all of its advantages, all-remote work isn't for everyone. Because it's non-traditional, all-remote work sometimes concerns investors, partners, and customers. It can also have disadvantages for potential employees (often senior, non-technical hires) depending on their lifestyle and work preferences.

For example, onboarding can be more difficult when you're remote, because it involves more self-learning and you're not physically with your new coworkers and fellow new hires. This can cause [the first month](https://www.linkedin.com/pulse/transition-remote-work-1-month-casey-allen-shobe/) in a remote role to feel lonely, especially if you're transitioning from a traditional office setting. Remote settings can also cause a breakdown in communication skills if organizations aren't intentional about creating ways for their people to stay connected. 

Some may also find it difficult to work in the same setting as they live and sleep, because a dedicated workspace helps to switch the context from their home life to work. 

The global nature of a company like GitLab can also create challenges for the organization and employees related to differences in currency and tax requirements around the world.

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/NoFLJLJ7abE" frameborder="0" allowfullscreen="true"> </iframe>
</figure>


## Why is this possible now?

All-remote work wouldn't be possible without the constant evolution of technology, and the tools that enable this type of work are continuously being developed and improved. 

We aren't just seeing these impacts for all-remote companies. In fact, in some organizations with large campuses, employees will routinely do video calls instead of spending 10 minutes to go to a different building.

Here are some of the key factors that make all-remote work possible: 

* Faster internet everywhere - 100Mb/s+ cable, 5GHz Wifi, 4G cellular
* Video call software - Google Hangouts, Zoom
* Virtual offices - sococo.com
* Mobile technology - Everyone has a computer in their pocket
* Evolution of speech-to-text conversion software - more accurate and faster than typing
* Messaging apps - Slack, Mattermost, Zulip
* Issue trackers - Trello, GitHub issues, GitLab issues
* Suggestions - GitHub Pull Requests, GitLab Merge Requests
* Static websites - GitHub Pages, GitLab Pages
* English proficiency - More people are learning English
* Increasing traffic congestion in cities

## What "all remote" does not mean

Let's address some of the common misconceptions about all-remote work. 

First things first: An all-remote company means there is *no* office or headquarters where multiple people are based. The only way to not have people in a satellite office is not to have a main office.

The terms "remote" and "distributed" are often used interchangeably, but they're not quite the same. We prefer the term "remote" because "distributed" suggests multiple physical offices. 
"Remote" is also the [most common term](https://www.google.com/search?ei=4IBsXKnLDIGRggftuqfAAQ&q=distributed+companies&oq=distributed+companies&gs_l=psy-ab.12...0.0..5177...0.0..0.0.0.......0......gws-wiz.6xnu76aJWr4) to refer to the absence of a physical workspace, and being able to do your job from anywhere.

For employees, being part of an all-remote company does not mean working independently or being isolated, because it's not a substitue for human interaction. 
Technology allows us to stay closely in touch with our teams, whether asychronously in text or in real time with high-fidelity conversations through video.
Teams should collaborate closely, communicate often, and feel like valuable members of a larger team.

Working remotely also doesn't mean you're physically constrained to home. 
You're free to work wherever you want. That could be at home with family, a coffee shop, a coworking space, or your local library while your little one is enjoying storytime. 
You can have frequent video chats or virtual pairing sessions with co-workers throughout the day, and you can even meet up with other coworkers to work together in person if you're located near each other.

At the organizational level, "all-remote" does not mean simply offshoring work. Instead, it means you're able to hire the best talent from all around the world. 
It's also not a management paradigm. You still have a hierarchical organization, but with a focus on output instead of input.

All in all, remote is fundamentally about _freedom_ and _individual choice_. At GitLab, we [value your results](/handbook/values/#results), not where you get your work done.

## How to build and manage a remote team 

<iframe width="560" height="315" src="https://www.youtube.com/embed/tSp5se9BudA" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### Facilitate informal communication

Making social connections with coworkers is important to building trust within an organization. All-remote companies need to faciliate these interactions. Here are some ways we faciliate informal interactions at GitLab:

- [Contribute Unconference](/company/culture/contribute/): An in-person, week-long event where we bring the entire company together in one location to get to know each other better.
- [Group conversations](/handbook/people-operations/group-conversations/): Four times a week the company gets together virtually to discuss an area of the business. Slides are provided for context but not presented.
- [Breakout calls](/handbook/communication/#breakout-call): Following the company call, everyone breaks out into small groups for 10-15 minutes to talk about non-work-related topics.
- [Coffee chats](/company/culture/all-remote/#coffee-chats): More detail below. 
- [Travel stipend](/handbook/incentives/#visiting-grant): The travel stipend encourages team members to visit each other by covering transportation costs up to $150 per person they visit.
- Local meetups: Co-located team members are encouraged to organize their own meetups, whether it's a coworking space or getting dinner together. 
- [CEO house](/handbook/ceo/#house): Team members can get together in Utrecht, Netherlands, at the CEOs AirBnB, free of charge. 


## What Have We Learned about Remote Working?

How do we manage our entire [team](/company/team/) remotely? Sid Sijbrandij, CEO, shares the [secrets to managing 200 employees in 200 locations](https://www.youtube.com/watch?v=e56PbkJdmZ8).

Our policy of remote work comes from our [value](/handbook/values/) of boring solutions and was a natural evolution of team members choosing to work from home. Remote work allowed for the development of our publicly viewable [handbook](/handbook/). We like efficiency and do not like having to explain things twice.

In on-site companies they take processes, camaraderie, and culture for granted and have it develop organically. In an all-remote company you have to organize it, this is hard to do but as you scale it becomes more efficient while the on-site organic approach fizzles out.

We have had success bonding with our coworkers in real life through our [Summits](/company/culture/contribute/) that are organized every 9 months and our [Visiting Grants](/handbook/incentives/#sts=Visiting grant).

We have a daily [team call](/handbook/communication/#team-call) to talk about what we did outside of work. There is also a daily [Group Conversation](/handbook/people-operations/group-conversations/) in which one department shares their progress and challenges.

Our [expense policy](/handbook/spending-company-money/) provides for equipment, internet service, office space if they want that, and a local holiday party.

We encourage people to have [coffee chats](https://work.qz.com/1147877/remote-work-why-we-put-virtual-coffee-breaks-in-our-company-handbook/) and do a [monthly AMA with the CEO](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/2649/diffs).

We have an extensive [onboarding template](https://gitlab.com/gitlab-com/people-ops/employment/blob/master/.gitlab/issue_templates/onboarding.md) for new hires and organize a [GitLab 101](/company/culture/gitlab-101/) to ask questions.

## Tips for Working Remotely

Arguably the biggest advantage of working remotely and asynchronously is the
flexibility it provides. This makes it easy to **combine** work with your
personal life although it might be difficult to find the right **balance**.
This can be mitigated by either explicitly planning your time off or plan when
you do work. When you don't work it is recommended to make yourself unavailable
by turning off Slack and closing down your email client. Coworkers should
allow this to work by abiding by the [communication guidelines](/2016/03/23/remote-communication#asynchronous-communication-so-everyone-can-focus).

If you worked at an office before, now you lack a default group to go out to
lunch with. To look at it from a different perspective, now you can select who
you lunch with and who you do not lunch with. Haven't spoken to a good friend in
a while? Now you can have lunch together.

### Practical tips

1. People don't have to say when they are working.
1. Working long hours or weekends is not encouraged nor celebrated.
1. Use screenshots in an issue tracker instead of a whiteboard, ensuring that everyone at any time can follow the thought process.
1. Encourage non-work related communication (talking about private life on a team call).
1. Encourage group video calls for [bonding](/2015/04/08/the-remote-manifesto/).
1. Encourage [video calls](/2015/04/08/the-remote-manifesto/) between people (10 as part of onboarding).
1. Periodic summits with the whole company to get to know each other in an informal setting.
1. Encourage [teamwork and saying thanks](https://about.gitlab.com/2015/04/08/the-remote-manifesto/).
1. Assign new hires a buddy so they have someone to reach out to.
1. Allow everyone in the company to view and edit every document.
1. Every document is always in draft, don't wait with sharing it until it is done.
1. Encourage people to write information down.

### Coffee Chats

Understanding that working remotely leads to mostly work-related conversations
with fellow GitLabbers, everyone is encouraged to dedicate **a few hours a week**
to having social calls with any teammate - get to know who you work with,
talk about everyday things and share a coffee, tea, or your favorite beverage. We want you to make
friends and build relationships with the people you work with to create a more comfortable,
well-rounded environment. The Coffee Chats are different from the
[Random Room](/handbook/communication/#random-room) video chat, they are meant to give you the option
to have 1x1 calls with specific teammates who you wish to speak with and is not a
random, open-for-all channel but a conversation between two teammates.

You can join the #donut_be_strangers Slack channel to be paired with a random team member for a coffee chat. The "Donut" bot will automatically send a message to two people in the channel every other Monday. Please schedule a chat together, and Donut will follow up for feedback.

Of course, you can also directly reach out to your fellow GitLabbers to schedule a coffee chat in the #donut_be_strangers Slack channel or via direct message.

### Tips on Ergonomic Working

The goal of [office ergonomics](http://ergo-plus.com/office-ergonomics-10-tips-to-help-you-avoid-fatigue/) is to design your office work station so that it fits you and allows for a comfortable working environment for maximum productivity and efficiency. Since we all work from home offices, GitLab wants to ensure that each team member has the [supplies](/handbook/spending-company-money/) and knowledge to create an ergonomic home office.

Below are some tips from the [Mayo Clinic](http://www.mayoclinic.org/healthy-lifestyle/adult-health/in-depth/office-ergonomics/art-20046169) how on how arrange your work station. If however, you develop any pains which you think might be related to your working position, please visit a doctor.

- **Chair**: Choose a chair that supports your spinal curves. Adjust the height of your chair so that your feet rest flat on the floor or on a footrest and your thighs are parallel to the floor. Adjust armrests so your arms gently rest on them with your shoulders relaxed.
- **Keyboard and mouse:** Place your mouse within easy reach and on the same surface as your keyboard. While typing or using your mouse, keep your wrists straight, your upper arms close to your body, and your hands at or slightly below the level of your elbows. Use keyboard shortcuts to reduce extended mouse use. If possible, adjust the sensitivity of the mouse so you can use a light touch to operate it. Alternate the hand you use to operate the mouse by moving the mouse to the other side of your keyboard. Keep regularly used objects close to your body to minimize reaching. Stand up to reach anything that can't be comfortably reached while sitting.
- **Telephone**: If you frequently talk on the phone and type or write at the same time, place your phone on speaker or use a headset rather than cradling the phone between your head and neck.
- **Footrest**: If your chair is too high for you to rest your feet flat on the floor — or the height of your desk requires you to raise the height of your chair — use a footrest. If a footrest is not available, try using a small stool or a stack of sturdy books instead.
- **Desk**: Under the desk, make sure there's clearance for your knees, thighs and feet. If the desk is too low and can't be adjusted, place sturdy boards or blocks under the desk legs. If the desk is too high and can't be adjusted, raise your chair. Use a footrest to support your feet as needed. If your desk has a hard edge, pad the edge or use a wrist rest. Don't store items under your desk. GitLab recommends having an adjustable standing desk to avoid any issues.
- **Monitor**: Place the monitor directly in front of you, about an arm's length away. The top of the screen should be at or slightly below eye level. The monitor should be directly behind your keyboard. If you wear bifocals, lower the monitor an additional 1 to 2 inches for more comfortable viewing. Place your monitor so that the brightest light source is to the side.

### Health and Fitness

It is sometimes hard to stay active when you work from your home. GitLab wants to ensure each team member takes care of themselves and dedicates time to stay healthy. Here are some tips that can help to stay healthy and active. You can also join the Slack channel `fitlab` to discuss your tips, challenges, results, etc. with other team members.

1. Try to step away from your computer and stretch your body every hour.
1. To avoid "Digital Eye Strain" follow the [20-20-20 Rule](https://www.healthline.com/health/eye-health/20-20-20-rule#definition). Every 20 minutes look into the distance (at least 20 feet/6 meters) for 20 seconds.
1. There are apps that will remind you to take a break or help you with your computer posture:
 - [PostureMinder](http://www.postureminder.co.uk/)(Windows)
 - [Time Out](https://itunes.apple.com/us/app/time-out-break-reminders/id402592703?mt=12)(macOS)
 - [Awareness](http://iamfutureproof.com/tools/awareness/)(macOS)
 - [SafeEyes](http://slgobinath.github.io/SafeEyes/)(GNU/Linux)
1. Move every day
  - Even when it can be hard to force yourself to move when working the whole day from your home try to go for a walk or do a short excersise for at least 15 minutes / day.
  - There are multiple activities that can be done within a short amount of time like rope jumping, lifting kettlebells, push-ups or sit-ups. It might also help to split the activity into multiple shorter sessions. You can use an app that helps you with the workout, e.g., [7 minute workout](https://7minuteworkout.jnj.com/).
1. Try to create a repeatable schedule that is consistent and try to make a habit out of it. Make sure you enjoy the activity.
1. It can also help to create a goal or challenge for yourself, e.g., registering for a race can force you to excercise.
1. Eat less refined sugar and simple carbs, eat complex carbs instead. Try to eat more vegetables. Don't go to the kitchen to eat something every 15 minutes (it’s a trap!). Keep junk food out of your house.
1. Have a water bottle with you at your desk. You will be more inclined to drink if it's available at all times.
1. Sleep well and take a nap during the day if you need one.


## Remote work in the news

1.  Inc. - [Case Closed: Work-From-Home Is the World's Smartest Management Strategy](https://www.inc.com/geoffrey-james/case-closed-work-from-home-is-worlds-smartest-management-strategy.html)
1. HackerNoon - [Remote work doesn't scale... or does it?](https://hackernoon.com/remote-work-doesnt-scale-or-does-it-4a72ce2bb1f3) 
1. GitLab Magazine - [How to keep healthy communication habits in remote teams](https://medium.com/gitlab-magazine/how-to-keep-healthy-communication-habits-in-remote-teams-a19eca371952)
1. remotehabits.com - [Remote work Interviews](https://remotehabits.com/)
1. Medium - [On-premise tribes in shiny caves and the evoluation of Remote as a Service](https://medium.com/understanding-as-a-service-uaas/on-premise-people-and-shiny-caves-remote-as-a-service-97cff86382b6)